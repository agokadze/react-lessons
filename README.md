# React/Redux Lessons

[![](https://d2eip9sf3oo6c2.cloudfront.net/tags/images/000/000/026/thumb/react.png )](https://egghead.io/browse/frameworks/react)

#### Egghead.io Tutorials

* [Start Learning React](https://egghead.io/courses/start-learning-react) -> [react-app](https://bitbucket.org/agokadze/react-lessons/src/master/react-app/?at=master)
* [Getting Started with Redux](https://egghead.io/courses/getting-started-with-redux) -> [redux-app](https://bitbucket.org/agokadze/react-lessons/src/master/redux-app/?at=master)
* [Building React Applications with Idiomatic Redux](https://egghead.io/courses/building-react-applications-with-idiomatic-redux) -> [idiomatic-redux-app](https://bitbucket.org/agokadze/react-lessons/src/master/idiomatic-redux-app/?at=master)


# Running

To start a dev server at `localhost:3000` run: 

```
cd [react-app|redux-app|idiomatic-redux-app]
npm install
npm start
```

Git repository of this projects contains tags for every tutorial.

### See all tags

Run `git tag` or `git tag -n` with messages for list of all available tags

### Checkout repo at tag

Run `git checkout <tagname>` to move your current branch to a state at specified
tag.

### Checking what you're missing

Run `git diff <tagname> <course-last-tagname>` to see how is `course-last-tagname` state of the repo different
from the `tagname` state.

